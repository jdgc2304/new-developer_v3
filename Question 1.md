# This doc provides the requested information for question #1.

I last used **RTFM** `yesterday`. My current team and I have found it to be an invaluable tool for defining requirements and maintaining a centralized location for task information (we use a Kanban board in Trello to define requirements and keep it as our 'manual'). It has greatly improved our efficiency and asynchronous communication becoming a place to return to when there is confusion or doubts.

**LMGTFY** is a tool I use daily. Given the cross-team environment we work in, there are often terms and technicalities that not everyone understands. LMGTFY is necessary nowadays for practically everything tech realted as it helps bridge the gap of understanding between multiple people without requiring tech knowledge.

As for my operating system, I am currently using `Windows / Linux`.

In terms of programming languages, I am proficient in `NodeJS / Javascript / Typescript` & `Python`.

I hope this information is helpful. Please let me know if you need any further details.

Best regards,
Juan Daniel
